all: libEGL.so

clean:
	-rm libEGL.so
	-rm es2gears

libEGL.so: wrap.c
	gcc -g -I/usr/include/libdrm -fPIC -shared -o libEGL.so wrap.c -ldl -ldrm -lgbm

es2gears: test/es2gears.c
	gcc test/eglut.c test/eglut_x11.c test/es2gears.c -o es2gears -lGLESv2 -lm -lX11 -lEGL

test: libEGL.so es2gears
	LD_PRELOAD=./libEGL.so ./es2gears
