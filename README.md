# drmegl-wrapper

This is a small wrapper to allow running some X 3D applications through the DRM API.

So far, this is only known to work with `es2gears`.

It also works with `es2tri` when the height and width assertions are removed and the redraw variable is initialized to 1 rather than 0.

# Testing:

### WARNING: This may kill X or lock up your system. Save anything you care about before running this!

Note that you'll need to have an X server running, but Xdummy should work.

Switch to a virtual terminal with `Ctrl-Alt-F2`.

Log in, go to the directory for this repo then test it with:

```bash
DISPLAY=:0 LIBEGL=/path/to/gles/libEGL.so make test
```

If everything works, you should see some spinning gears on screen. Hit `^C` to stop it.

Note that I have included the `es2gears` source with this as for some reason upstream `es2gears` doesn't work for me.

To test other programs:

```bash
DISPLAY=:0 LIBEGL=/path/to/gles/libEGL.so LD_PRELOAD=/path/to/wrapper/libEGL.so supertuxkart # Probably won't work
```

With gl4es:

```bash
DISPLAY=:0 LIBEGL=/path/to/gles/libEGL.so LD_LIBRARY_PATH=/path/to/gl4es:/path/to/drmegl-wrapper:/path/to/gles/libs glxgears
```

