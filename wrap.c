/*
 * Copyright (c) 2012 Arvin Schnell <arvin.schnell@gmail.com>
 * Copyright (c) 2012 Rob Clark <rob@ti.com>
 * Copyright (c) 2017 Miouyouyou <Myy> <myy@miouyouyou.fr>
 * Copyright (c) 2019 Icecream95 <ixn@disroot.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* Based on a egl cube test app originally written by Arvin Schnell */

#define _GNU_SOURCE
#include <dlfcn.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <xf86drm.h>
#include <xf86drmMode.h>
#include <gbm.h>

#define GL_GLEXT_PROTOTYPES 1
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <assert.h>

static void* egl_handle = NULL;

//#define USE_DLOPEN 1

// Change this for your sysytem
#define LIBEGL "/usr/lib/libEGL.so"

#ifdef USE_DLOPEN
#define EGLFN(name) dlsym(egl_handle, name)
#else
#define EGLFN(name) dlsym(RTLD_NEXT, name)
#endif

static struct {
    EGLDisplay display;
    EGLConfig config;
    EGLContext context;
    EGLSurface surface;
    GLuint program;
    GLint modelviewmatrix, modelviewprojectionmatrix, normalmatrix;
    GLuint vbo;
    GLuint positionsoffset, colorsoffset, normalsoffset;
} gl;

static struct {
    struct gbm_device *dev;
    struct gbm_surface *surface;
} gbm;

static struct {
    int fd;
    drmModeModeInfo *mode;
    uint32_t crtc_id;
    uint32_t connector_id;
} drm;

struct drm_fb {
    struct gbm_bo *bo;
    uint32_t fb_id;
};

static uint32_t find_crtc_for_encoder(const drmModeRes *resources,
				      const drmModeEncoder *encoder) {
    int i;

    for (i = 0; i < resources->count_crtcs; i++) {
        /* possible_crtcs is a bitmask as described here:
         * https://dvdhrm.wordpress.com/2012/09/13/linux-drm-mode-setting-api
         */
        const uint32_t crtc_mask = 1 << i;
        const uint32_t crtc_id = resources->crtcs[i];
        if (encoder->possible_crtcs & crtc_mask) {
            return crtc_id;
        }
    }

    /* no match found */
    return -1;
}

static uint32_t find_crtc_for_connector(const drmModeRes *resources,
					const drmModeConnector *connector) {
    int i;

    for (i = 0; i < connector->count_encoders; i++) {
        const uint32_t encoder_id = connector->encoders[i];
        drmModeEncoder *encoder = drmModeGetEncoder(drm.fd, encoder_id);

        if (encoder) {
            const uint32_t crtc_id = find_crtc_for_encoder(resources, encoder);

            drmModeFreeEncoder(encoder);
            if (crtc_id != 0) {
                return crtc_id;
            }
        }
    }

    /* no match found */
    return -1;
}

static void die()
{
    abort();
}

static void init_drm()
{
    drmModeRes *resources;
    drmModeConnector *connector = NULL;
    drmModeEncoder *encoder = NULL;
    int i, area;

    // TODO: Use environment variable
    drm.fd = open("/dev/dri/card0", O_RDWR);
	
    if (drm.fd < 0) {
        printf("DRMEGL: Could not open drm device!\n"
               "DRMEGL:  Try adding yourself to the \"video\" group,\n"
               "DRMEGL:  or running chmod a+rw /dev/dri/card0\n");
        die();
    }

    resources = drmModeGetResources(drm.fd);
    if (!resources) {
        printf("DRMEGL: drmModeGetResources failed: %s\n", strerror(errno));
        die();
    }

    /* find a connected connector: */
    for (i = 0; i < resources->count_connectors; i++) {
        connector = drmModeGetConnector(drm.fd, resources->connectors[i]);
        if (connector->connection == DRM_MODE_CONNECTED) {
            /* it's connected, let's use this! */
            break;
        }
        drmModeFreeConnector(connector);
        connector = NULL;
    }

    if (!connector) {
        /* we could be fancy and listen for hotplug events and wait for
         * a connector..
         */
        printf("DRMEGL: No connected DRM connector!\n");
        die();
    }

    /* find prefered mode or the highest resolution mode: */
    for (i = 0, area = 0; i < connector->count_modes; i++) {
        drmModeModeInfo *current_mode = &connector->modes[i];

        if (current_mode->type & DRM_MODE_TYPE_PREFERRED) {
            drm.mode = current_mode;
        }

        int current_area = current_mode->hdisplay * current_mode->vdisplay;
        if (current_area > area) {
            drm.mode = current_mode;
            area = current_area;
        }
    }

    if (!drm.mode) {
        printf("DRMEGL: Could not find DRM mode!\n");
        die();
    }

    /* find encoder: */
    for (i = 0; i < resources->count_encoders; i++) {
        encoder = drmModeGetEncoder(drm.fd, resources->encoders[i]);
        if (encoder->encoder_id == connector->encoder_id)
            break;
        drmModeFreeEncoder(encoder);
        encoder = NULL;
    }

    if (encoder) {
        drm.crtc_id = encoder->crtc_id;
    } else {
        uint32_t crtc_id = find_crtc_for_connector(resources, connector);
        if (crtc_id == 0) {
            printf("DRMEGL: No DRM crtc found!\n");
            die();
        }

        drm.crtc_id = crtc_id;
    }

    drm.connector_id = connector->connector_id;
}

static int init_gbm()
{
    gbm.dev = gbm_create_device(drm.fd);
    gbm.surface = gbm_surface_create(gbm.dev,
                                     drm.mode->hdisplay, drm.mode->vdisplay,
                                     GBM_FORMAT_XRGB8888,
                                     GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
    if (!gbm.surface) {
        printf("DRMEGL: Failed to create gbm surface\n");
        die();
    }
}

static void init_gl()
{
    void* (*eglGetPlatformDisplay)(EGLenum, EGLDisplay, const EGLAttrib*) = EGLFN("eglGetPlatformDisplay");
    if (!eglGetPlatformDisplay) {
        eglGetPlatformDisplay = EGLFN("eglGetPlatformDisplayEXT");
        if (!eglGetPlatformDisplay)
            die();
    }
    else {
        printf("DRMEGL: Note: Found eglGetPlatformDisplay\n");
    }

    gl.display = eglGetPlatformDisplay(EGL_PLATFORM_GBM_KHR, gbm.dev, NULL);
}

static void drm_fb_destroy_callback(struct gbm_bo *bo, void *data)
{
    struct drm_fb *fb = data;
    struct gbm_device *gbm = gbm_bo_get_device(bo);

    if (fb->fb_id)
        drmModeRmFB(drm.fd, fb->fb_id);

    free(fb);
}

static struct drm_fb * drm_fb_get_from_bo(struct gbm_bo *bo)
{
    struct drm_fb *fb = gbm_bo_get_user_data(bo);
    uint32_t width, height, stride, handle;
    int ret;

    if (fb)
        return fb;

    fb = calloc(1, sizeof *fb);
    fb->bo = bo;

    width = gbm_bo_get_width(bo);
    height = gbm_bo_get_height(bo);
    stride = gbm_bo_get_stride(bo);
    handle = gbm_bo_get_handle(bo).u32;

    ret = drmModeAddFB(drm.fd, width, height, 24, 32, stride, handle, &fb->fb_id);
    if (ret) {
        printf("DRMEGL: Failed to create fb: %s\n", strerror(errno));
        free(fb);
        die();
    }

    gbm_bo_set_user_data(bo, fb, drm_fb_destroy_callback);

    return fb;
}

static fd_set fds;

EGLAPI EGLDisplay EGLAPIENTRY eglGetDisplay (EGLNativeDisplayType display_id)
{
    static int setup_gbm = 0;
    if (!setup_gbm) {
        printf("DRMEGL: Init\n");
        setup_gbm = 1;
        char *libegl = getenv("LIBEGL");
        if (!libegl)
            libegl = LIBEGL;
#ifdef USE_DLOPEN
        egl_handle = dlopen(libegl, RTLD_LAZY);
#endif
        init_drm();
        FD_ZERO(&fds);
        FD_SET(0, &fds);
        FD_SET(drm.fd, &fds);
        init_gbm();
    }
    init_gl();
    return gl.display;
}

static void page_flip_handler(int fd, unsigned int frame,
		  unsigned int sec, unsigned int usec, void *data)
{
	int *waiting_for_flip = data;
	*waiting_for_flip = 0;
}

EGLAPI EGLSurface EGLAPIENTRY eglCreateWindowSurface (EGLDisplay dpy, EGLConfig config, EGLNativeWindowType win, const EGLint *attrib_list)
{
    EGLSurface (*fn_eglCreate)(EGLDisplay, EGLConfig, EGLNativeWindowType, const EGLint*);
    fn_eglCreate = EGLFN("eglCreateWindowSurface");
    return fn_eglCreate(dpy, config, gbm.surface, NULL);
}

static struct gbm_bo *bo;

static int frames = 0;

EGLAPI EGLBoolean EGLAPIENTRY eglSwapBuffers (EGLDisplay dpy, EGLSurface surface)
{
    frames++;

    EGLBoolean (*fn_eglSwap)(EGLDisplay, EGLSurface);
    fn_eglSwap = EGLFN("eglSwapBuffers");
    EGLBoolean res = EGL_TRUE;

    res = fn_eglSwap(dpy, surface);

    // Uncomment to disable vsync and enable bad tearing and/or flickering:
//    return res;

    struct gbm_bo *next_bo = gbm_surface_lock_front_buffer(gbm.surface);
    struct drm_fb *fb = drm_fb_get_from_bo(next_bo);

    drmEventContext evctx = {
        .version = DRM_EVENT_CONTEXT_VERSION,
        .page_flip_handler = page_flip_handler,
    };

    int waiting_for_flip = 1;

    int ret;
    ret = drmModePageFlip(drm.fd, drm.crtc_id, fb->fb_id,
                          DRM_MODE_PAGE_FLIP_EVENT, &waiting_for_flip);
    if (ret) {
        printf("failed to queue page flip: %s\n", strerror(errno));
        return -1;
    }

    while (waiting_for_flip) {
        ret = select(drm.fd + 1, &fds, NULL, NULL, NULL);
        if (ret < 0) {
            printf("select err: %s\n", strerror(errno));
            return ret;
        } else if (ret == 0) {
            printf("select timeout!\n");
            return -1;
        } else if (FD_ISSET(0, &fds)) {
            printf("user interrupted!\n");
            break;
        }
        drmHandleEvent(drm.fd, &evctx);
    }

    res = fn_eglSwap(dpy, surface);
    res = fn_eglSwap(dpy, surface);

    gbm_surface_release_buffer(gbm.surface, bo);
    bo = next_bo;
    return res;
}

EGLAPI EGLBoolean EGLAPIENTRY eglMakeCurrent (EGLDisplay dpy, EGLSurface draw, EGLSurface read, EGLContext ctx)
{
    int r;
    struct drm_fb *fb;
    
    EGLBoolean (*fn_eglCurrent)(EGLDisplay, EGLSurface, EGLSurface, EGLContext);
    EGLBoolean (*fn_eglSwap)(EGLDisplay, EGLSurface);

    fn_eglCurrent = EGLFN("eglMakeCurrent");
    EGLBoolean ret = fn_eglCurrent(dpy, draw, read, ctx);

    fn_eglSwap = EGLFN("eglSwapBuffers");

    gl.surface = draw;

    fn_eglSwap(gl.display, gl.surface);
    bo = gbm_surface_lock_front_buffer(gbm.surface);
    if (!bo) {
        printf("DRMEGL: Note: GBM buffer object NULL in eglMakeCurrent\n");
        return ret;
    }
    fb = drm_fb_get_from_bo(bo);
    gbm_surface_release_buffer(gbm.surface, bo);

    printf("DRMEGL: Doing modeset\n");
    r = drmModeSetCrtc(drm.fd, drm.crtc_id, fb->fb_id, 0, 0,
                         &drm.connector_id, 1, drm.mode);
    if (r) {
        printf("DRMEGL: Failed to set mode: %s\n", strerror(errno));
        die();
    }

    return ret;
}

// A little hack to stop es2gears from complaining about the window size

#include <X11/Xlib.h>
#include <X11/Xutil.h>

XVisualInfo *XGetVisualInfo(Display *display, long vinfo_mask, XVisualInfo *vinfo_template, int *nitems_return)
{
    XVisualInfo* (*fn_XGetVis)(Display*, long, XVisualInfo*, int*);
    fn_XGetVis = EGLFN("XGetVisualInfo");
    printf("DRMEGL: Using XGetVisualInfo hack\n");
    return fn_XGetVis(display, 0, NULL, nitems_return);
}
